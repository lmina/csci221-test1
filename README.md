Midterm for Software Development using only one site (Dr. Eckroth's notes/aid).
Language: C++
Code:
*Note: Anything in quotations comes directly from the test instructions on the [site](http://csci221.artifice.cc/tests/test-1-fall2016.html).

**Problem 1:** "Update the linked list code from class ... so that the list can store any kind of value in any node. Be sure there are no memory leaks, including in your main.cpp"

**Problem 2:** Using the tree code created in a previous class,

1. "Write a function that finds the max depth of the tree. The max depth is the longest number of steps from the root to a leaf. The depth of a tree with just one root node and no children is 1. Add two tests for this function in main.cpp. Be sure there are no memory leaks (the starting code has no leaks).

2. Write a function that counts and returns the number of nodes in the tree. Add two more tests to main.cpp to test this function. Be sure there are no memory leaks. "

**Problem 3:** " Write a “Vector” class that stores double values in an array (do not use a linked list or anything else but an array for the underlying storage). Create these class functions, plus any others you need:

* a default constructor, Vector()
* a constructor that specifies the initial size of the vector, Vector(int n)
* a set-value function, void set(int idx, double val), that sets the position idx in the vector to the val given; if the position is larger than the current size of the array, grow the array to accomodate
* a get-value function, double get(int idx) const, that returns the value at position idx; if the position is invalid (too large or negative), just return anything you want (the behavior is undefined)
* a function that returns the array size, int size() const, which may have grown since it was first constructed due to the set function

Create a proper .cpp and .h separation of code for the class. "
The main.cpp file is that given by the professor in the test page.
"You should have no memory leaks, and the main function should print correct values (it’s self-explanatory). Also write a Makefile, with a clean target as well as the usual targets."