
class Vector {
    private:
        double* vals;
        int len;
       

    public:
        Vector();
        Vector(int length);
        ~Vector();
        void set(int idx, double val);
        double get(int idx) const;
        int size() const;
        bool remove(int idx) const;
};
