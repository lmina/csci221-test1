#include <iostream>
#include "vector.h"
using namespace std;

Vector::Vector() {
    len = 10;
    vals = new double[len];
}

Vector::Vector(int length) {
    this->len = length;
    vals = new double[len];
}

void Vector::set(int idx, double val) {
    if(idx > -1) {
        if(idx > len) {
            int newSize = len;
            while(idx > newSize) {
                newSize *= 2;
            }

            double* newVals = new double[len];
            for (int s = 0; s < len; s++) {
                newVals[s] = vals[s];
            }

            vals = new double[newSize];
            for (int ns = 0; ns < len; ns++) {
                vals[ns] = newVals[ns];
            }
            delete[] newVals;
        }
        vals[idx] = val;
    }
}

double Vector::get(int idx) const {
    if(idx < len) {
        return vals[idx];
    }
    return -2.999;
}

int Vector::size() const {
    return len;
}

Vector::~Vector() {
    delete[] vals;
}

