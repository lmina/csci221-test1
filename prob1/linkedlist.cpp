
#include <cassert>
using namespace std;

#define NULL 0

#include "linkedlist.h"

Node::Node(void* _val) {
    val = _val;
    next = NULL;
}

void Node::setNext(Node* _next) {
    next = _next;
}

Node* Node::getNext() {
    return next;
}

void* Node::getVal() {
    return val;
}

LinkedList::LinkedList() {
    head = tail = NULL;
    len = 0;
}

void LinkedList::add(void* val) {
    Node* n = new Node(val);
    len++;
    if(head == NULL) head = n;
    if(tail != NULL) tail->setNext(n);
    tail = n;
}

void LinkedList::remove(int idx) {
    if(idx > len) { return; }
    Node *n = head;
    Node *pn = NULL;
    while(idx > 0 && idx < len) {
        pn = n;
        n = n->getNext();
        idx--;
    }
    pn->setNext(n->getNext());
    delete n;
    len--;
}

void* LinkedList::valAt(int idx) {
    assert(idx <= len && idx >= 0);
    Node *n = head;
    while(idx > 0) {
        n = n->getNext();
        idx--;
    }
    void* pn = n->getVal();
    delete n;
    return pn;
}

int LinkedList::length() {
    return len;
}

