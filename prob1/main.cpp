
#include <iostream>
using namespace std;

#include "linkedlist.h"
#include "tree.h"

int main()
{
    LinkedList *list = new LinkedList();
    cout << "Length: " << list->length() << endl;
    int x = 52;
    void* px = (void*) &x;
    double y = 62.3;
    void* py = (void*) &y;
    Tree* z = new Tree ('z');
    void* pz = (void*) &z;

    list->add(px);
    list->add(py);
    list->add(pz);
    cout << "Length: " << list->length() << endl;
    cout << "Val at 0: " << *((int*)(list->valAt(0))) << "\n";
    cout << "Val at 1: " << *((double*)(list->valAt(1))) << "\n";
    Tree* t = (Tree*)(list->valAt(2));
    cout << "Val at 2: " << t->getVal();
 
    cout << endl;
    delete z;
    delete list;
    return 0;
}


