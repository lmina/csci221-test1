#include "tree.h"

#define NULL 0

Tree::Tree(char c) {
    this->c = c;
    numChildren = 0;
    children = NULL;
}

int Tree::getNumChildren() const {
    return numChildren;
}

bool Tree::addChild(Tree *child) {
    // create a new children array, copy all children into it, and add the new one
    Tree **childrenNew = new Tree*[++numChildren];
    // keep ++i. you are not expected to understand this.
    for(int i = 0; i < numChildren-1; ++i) {
        childrenNew[i] = children[i];
    }
    childrenNew[numChildren-1] = child; // save the star child into the children array
    if(children) delete[] children;
    children = childrenNew;
    return true;
}

Tree *Tree::getChild(int idx) const
{
    if(idx < 0 || idx >= numChildren)
        return NULL;
    return children[idx];
}

/*bool Tree::hasChild(Tree* child) const {
    if(child->getChild(0) != NULL) return true;
    return false;
}*/

char Tree::getVal() const {
    return c;
}

bool Tree::hasVal(char c) const {
    if(this->c == c) return true;
    for(int i = 0; i < numChildren; i++) {
        if(children[i]->hasVal(c)) {
            return true;
        }
    }
    return false;
}

char Tree::findMax() const {
    char max = c;
    for(int i = 0; i < numChildren; i++) {
        char cmax = children[i]->findMax();
        if(cmax > max) max = cmax;
    }
    return max;
}

//author: Luisa M
int Tree::maxDepth() const {
    int maxD = 0;
    int depth = 1;
    if(numChildren != 0) {
        for(int x = 0; x < numChildren; x++) {
            Tree* n = children[x];
            depth += n->maxDepth();
            if(depth > maxD) maxD = depth;
            depth = 2;
        }
    }
    return maxD;
}

//author: Luisa M
int Tree::numNodes() const {
    int nodes = 0;
    if(numChildren != 0) {
        nodes += numChildren;
        for(int x = 0; x < numChildren; x++) {
            Tree* n = children[x];
            nodes += n->numNodes();
        }
    }
    return nodes;
}

Tree::~Tree() {
    for(int i = 0; i < numChildren; i++) {
        delete children[i];
    }
    if(children) delete[] children;
}


