#include <iostream>
using namespace std;

#include "tree.h"

int main()
{
    Tree *mytree = new Tree('a');
    Tree *child1 = new Tree('b');
    Tree* child2 = new Tree('f');
    Tree* child3 = new Tree('g');
    mytree->addChild(child1); // now mytree has *responsibility* for the memory of child1
    mytree->addChild(new Tree('c'));
    mytree->addChild(new Tree('d'));
    cout << "max depth: (should be 2) " << mytree->maxDepth() << endl;
    cout << "nodes: (should be 3) " << mytree->numNodes() << endl;
    child1->addChild(new Tree('e'));
    child1->addChild(child2);
    child2->addChild(child3);
    cout << "max depth: (should be 4) " << mytree->maxDepth() << endl;
    cout << "nodes: (should be 6) " << mytree->numNodes() << endl;
    //cout << mytree->findMax() << "\n";
    //cout << "Num of children: " << mytree->getNumChildren() << endl;
    //cout << "Value at pos 2: " << mytree->getChild(2)->getVal() << endl;

    delete mytree; // will cause the destructor function to execute
    return 0;
}


